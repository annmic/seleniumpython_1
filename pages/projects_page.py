from selenium.webdriver.common.by import By


class ProjectsPage:
    def __init__(self, browser):
        self.browser = browser

    def click_add_project(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a.button_link[href="http://demo.testarena.pl/administration/add_project"]').click()

    def find_project(self, name):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(name)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()
