import time

from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver


def test_google_title():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.google.com/")
    assert "Google" in driver.title
    driver.quit()


def test_bing_title():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.bing.com/")
    driver.fullscreen_window()
    driver.maximize_window()
    assert "Bing" in driver.title
    time.sleep(2)
    driver.quit()


