from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time


def test_searching_in_bing():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony Bing
    browser.get('https://www.bing.com/')

    # Znalezienie paska wyszukiwania
    search_box_input = browser.find_element(By.CSS_SELECTOR, '#sb_form_q')
    # Znalezienie guzika wyszukiwania (lupki)
    search_button_input = browser.find_element(By.CSS_SELECTOR, '.search_svg')

    # Asercje że elementy są widoczne dla użytkownika
    assert search_box_input.is_displayed() is True
    assert search_button_input.is_displayed() is True

    # Szukanie Vistula University
    search_box_input.send_keys('Vistula University')
    search_button_input.click()

    #Sprawdzenie czy lista wyników jest dłuższa niż 2
    search_results = browser.find_elements(By.CSS_SELECTOR, '.b_algo')
    assert len(search_results) > 2

    # Sprawdzenie że pierwszy wynik ma tytuł 'Vistula University in Warsaw'
    title_results = browser.find_elements(By.CSS_SELECTOR, '.b_topTitle')
    first_title = title_results[0].text
    assert first_title == 'Home - Vistula University'

    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()
