import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.projects_page import ProjectsPage
from pages.new_project_page import NewProjectPage
from pages.project_info_page import ProjectInfoPage

from utils.random import RandomUtil

@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield browser
    browser.quit()

def test_final_selenium_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    projects_page = ProjectsPage(browser)
    projects_page.click_add_project()

    add_project = NewProjectPage(browser)
    project_prefix = RandomUtil.random_string(5)
    project_name = f"selenium_AM {project_prefix}"
    add_project.add_project(project_name, project_prefix)

    project_details = ProjectInfoPage(browser)
    project_details.project_confirmation()
    project_details.click_cockpit()
    cockpit_page.click_administration()
    projects_page.find_project(project_name)

    name = browser.find_element(By.LINK_TEXT, project_name)
    assert name.is_displayed() is True
    assert name.text == project_name

